/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */

import {DebugElement} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ComponentFixture, fakeAsync, flush, TestBed, tick, waitForAsync} from '@angular/core/testing';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {By} from '@angular/platform-browser';
import {Action, CollectionComponent, HeaderOption} from './collection.component';
import {ModelProperty} from '../../models/abstract.model';
import {CommonComponentModule} from '../common.component.module';
import { IconPosition } from '../button/button.component';

// % protected region % [Add any additional imports here] off begin
// % protected region % [Add any additional imports here] end

let defaultItemActions: Action[] = [
	// % protected region % [Override the default item actions here] off begin
	{
		label: 'View',
		icon: 'look',
		iconPos: IconPosition.TOP,
		showIcon: true,
		isAdditional: false
	},
	{
		label: 'Edit',
		icon: 'edit',
		iconPos: IconPosition.TOP,
		showIcon: true,
		isAdditional: false,
		disableOption: () => false
	},
	{
		label: 'Delete',
		icon: 'bin-delete',
		iconPos: IconPosition.TOP,
		showIcon: true,
		isAdditional: false,
		disableOption: () => false
	},
	// % protected region % [Override the default item actions here] end
];

