/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */

package springfish.validators;

import java.util.Set;
import java.util.stream.Stream;
import java.util.UUID;

import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import net.andreinc.mockneat.MockNeat;
import net.andreinc.mockneat.types.enums.StringType;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import springfish.entities.*;

import static org.junit.jupiter.api.Assertions.*;

@Tag("validation")
public class AttributeValidatorTest {

	private Validator validator;
	private MockNeat mockNeat;

	private Class getEntityClass(String entityName) throws Exception {
		switch (entityName) {
			default:
				throw new Exception(entityName + "must be a valid entity");
		}
	}

	private String getAlphanumericString(int length) {
		return mockNeat.strings().size(length).type(StringType.valueOf("ALPHA_NUMERIC")).get();
	}

	@BeforeEach
	void setup() {
		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		validator = factory.getValidator();
		//Multiple options are available for instantiating MockNeat.  MockNeat.threadLocal() is recommended in the docs
		mockNeat = MockNeat.threadLocal();
	}

}
