/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */
package springfish.configs.security.data;

import springfish.configs.security.helpers.AnonymousHelper;
import springfish.configs.security.helpers.RoleAndPrivilegeHelper;
import springfish.configs.security.repositories.UserRepository;
import springfish.entities.*;
import springfish.repositories.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.core.annotation.Order;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.*;

import static springfish.configs.security.helpers.RoleAndPrivilegeHelper.createOrUpdatePrivilege;

@Component
@Slf4j
@Order(1)
// % protected region % [Override your the selected profiles here] off begin
@Profile({"dev", "test"})
// % protected region % [Override your the selected profiles here] end
public class TestUsers implements ApplicationRunner {

	/*
	 * Base user repository
	 */
	private final UserRepository userRepository;
	private final RoleRepository roleRepository;

	private final PasswordEncoder passwordEncoder;

	@Autowired
	public TestUsers(
			UserRepository userRepository,
			RoleRepository roleRepository,
			PasswordEncoder passwordEncoder
	) {
		this.userRepository = userRepository;
		this.roleRepository = roleRepository;
		this.passwordEncoder = passwordEncoder;
	}

	@Override
	public void run(ApplicationArguments args) throws Exception {
		AnonymousHelper.runAnonymously(() -> {
			// % protected region % [Override the anonymous run logic here] off begin
			Map<String, RoleEntity> mappedRoles = new HashMap<>();
			Iterable<RoleEntity> roles = roleRepository.findAll();

			roles.forEach(roleEntity -> {
				mappedRoles.put(roleEntity.getName(), roleEntity);
			});

			setupSuperRoleAndAccount();
			setupTestAccounts(mappedRoles);
			// % protected region % [Override the anonymous run logic here] end
		});
	}

	private void setupSuperRoleAndAccount() {
		// Setup the super role and privilege, we add the ` ` to remove the chance of conflicts with other
		// user roles due to variation from the naming convention.
		final String superPrivilegeName = "ROLE_SUPER ADMIN";
		final String superRoleName = "SUPER ADMIN";

		Optional<RoleEntity> superRoleOpt = roleRepository.findByName(superRoleName);
		var superRoleEntity = superRoleOpt.orElseGet(() -> {
			var role = new RoleEntity();
			role.setName(superRoleName);
			return roleRepository.save(role);
		});

		Arrays.asList(RoleAndPrivilegeHelper.APPLICATION_ENTITY_NAMES).forEach((entityName) -> createOrUpdatePrivilege(
				superRoleEntity, entityName,
				// Do a text transform to be consistent with the security model defined ACLs
				superPrivilegeName + "_" + entityName.toUpperCase().replace("ENTITY", "_ENTITY"),
				true, true, true, true)
		);

		// We set the id to allow for accurate linking but separate it so that we can allow our lambda to consume final values
		var savedSuperRole = roleRepository.save(superRoleEntity);
		superRoleEntity.setId(savedSuperRole.getId());

		// Create a super user. A super user has full access to everything
		UserEntity superUser;
		if (userRepository.findByEmail("super@example.com").isEmpty()) {
			superUser = new UserEntity();
			superUser.setEmail("super@example.com");
			superUser.setUsername("super@example.com");
			superUser.setPassword(passwordEncoder.encode("password"));
			superUser.setIsArchived(false);
			superUser.setName("Super User");
		} else {
			superUser = userRepository.findByEmail("super@example.com").get();
		}

		superUser.setRoles(Collections.singletonList(superRoleEntity));

		userRepository.save(superUser);
	}

	/**
	 * Setup test accounts for ease of development.
	 */
	private void setupTestAccounts(Map<String, RoleEntity> roles) {


		// % protected region % [Add additional test users here] off begin
		// % protected region % [Add additional test users here] end
	}

	// % protected region % [Add any additional class methods here] off begin
	// % protected region % [Add any additional class methods here] end
}