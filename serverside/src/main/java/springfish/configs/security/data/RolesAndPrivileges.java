/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */
package springfish.configs.security.data;

import springfish.configs.security.helpers.AnonymousHelper;
import springfish.entities.RoleEntity;
import springfish.repositories.RoleRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.Optional;

import static springfish.configs.security.helpers.RoleAndPrivilegeHelper.createOrUpdatePrivilege;

// % protected region % [Add any additional imports here] off begin
// % protected region % [Add any additional imports here] end

/**
 * Setup the Roles and Privileges for the application
 */
@Component
@Slf4j
@Order(Ordered.HIGHEST_PRECEDENCE)
public class RolesAndPrivileges implements ApplicationRunner {

	private final RoleRepository roleRepository;

	@Autowired
	public RolesAndPrivileges(
			// % protected region % [Add any additional constructor parameters here] off begin
			// % protected region % [Add any additional constructor parameters here] end
			RoleRepository roleRepository
	) {
		// % protected region % [Add any additional constructor logic here] off begin
		// % protected region % [Add any additional constructor logic here] end

		this.roleRepository = roleRepository;

		// % protected region % [Add any additional constructor logic after the main body here] off begin
		// % protected region % [Add any additional constructor logic after the main body here] end
	}

	@Override
	public void run(ApplicationArguments args) throws Exception {
		// % protected region % [Add any additional pre-setup logic here] off begin
		// % protected region % [Add any additional pre-setup logic here] end

		AnonymousHelper.runAnonymously(this::setup);

		// % protected region % [Add any additional post-setup logic here] off begin
		// % protected region % [Add any additional post-setup logic here] end
	}

	/**
	 * Create default roles and privileges to be associated with users. Note that this method does not take anonymous
	 * roles into account. Instead it is deferred to {@link springfish.configs.security.filters.AuthenticationFilter}.
	 */
	private void setup() {
		// % protected region % [Add additional custom logic for the role setup] off begin
		// % protected region % [Add additional custom logic for the role setup] end
	}

	// % protected region % [Add any additional class methods here] off begin
	// % protected region % [Add any additional class methods here] end
}
