/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */
package springfish.configs.security.helpers;

import springfish.entities.PrivilegeEntity;
import springfish.entities.RoleEntity;

import java.util.UUID;

// % protected region % [Add any additional imports here] off begin
// % protected region % [Add any additional imports here] end

/**
 * Helper class for managing the roles and privileges that are restricted in {@link springfish.configs.security.SecurityConfig}
 */
public class RoleAndPrivilegeHelper {

	/**
	 * Create a privilege for the role
	 * If privilege is already exists, create a new one
	 *
	 * @param roleEntity    Related role entity
	 * @param entityName    Name of target entity
	 * @param privilegeName Name of the privilege entity
	 * @param allowCreate   whether allow to create
	 * @param allowRead     whether allow to read
	 * @param allowUpdate   whether allow to update
	 * @param allowDelete   whether allow to delete
	 */
	public static void createOrUpdatePrivilege(RoleEntity roleEntity, String entityName, String privilegeName,
											   Boolean allowCreate, Boolean allowRead, Boolean allowUpdate, Boolean allowDelete) {

		PrivilegeEntity privilegeEntity = roleEntity.getPrivileges().stream()
				.filter(privilege -> privilege.getName().equals(privilegeName))
				.findFirst().orElse(null);

		if (privilegeEntity == null) {
			privilegeEntity = new PrivilegeEntity();
			privilegeEntity.setId(UUID.randomUUID());
			privilegeEntity.setName(privilegeName);
			privilegeEntity.setTargetEntity(entityName);
			roleEntity.getPrivileges().add(privilegeEntity);
		}

		privilegeEntity.setAllowCreate(allowCreate);
		privilegeEntity.setAllowRead(allowRead);
		privilegeEntity.setAllowUpdate(allowUpdate);
		privilegeEntity.setAllowDelete(allowDelete);
	}

	public static final String[] APPLICATION_ENTITY_NAMES = {
	};

	// % protected region % [Add any additional class methods here] off begin
	// % protected region % [Add any additional class methods here] end
}


