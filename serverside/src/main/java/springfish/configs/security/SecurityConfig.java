/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */
package springfish.configs.security;

import org.springframework.http.HttpHeaders;
import springfish.configs.security.authorities.CustomGrantedAuthority;
import springfish.configs.security.filters.*;
import springfish.configs.security.repositories.XsrfTokenRepository;
import springfish.configs.security.services.*;
import springfish.repositories.*;
import com.google.common.collect.ImmutableList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.*;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.*;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.*;
import org.springframework.security.core.userdetails.*;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.*;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.web.cors.*;
import org.springframework.web.filter.CorsFilter;

import javax.servlet.http.HttpServletResponse;
import java.util.*;

// % protected region % [Add any additional imports here] off begin
// % protected region % [Add any additional imports here] end

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
	/**
	 * Whitelist URLs which will not require authentication when accessed. However, Spring by default will still require
	 * an {@link org.springframework.security.core.Authentication} object if no logged in is detected.
	 */
	public static final String[] AUTH_WHITELIST = {
			// % protected region % [Add any additional whitelist URL here] off begin
			// % protected region % [Add any additional whitelist URL here] end
	};

	/**
	 * Similar to {@link #AUTH_WHITELIST} but for the specific purpose of allowing access during development only.
	 */
	public static final String[] DEV_AUTH_WHITELIST = {
			// % protected region % [Add any additional development whitelisted URL here] off begin
			// % protected region % [Add any additional development whitelisted URL here] end

			"/h2/**",
			"/actuator",
			"/actuator/**",
			"/voyager",
			"/altair",
			"/graphiql",
			"/swagger-ui.html",
	};

	/**
	 * Allow anonymous access for requests using the HTTP GET verb for these endpoints.
	 */
	public static final String[] ANONYMOUS_GET_AUTH_WHITELIST = {

			/* Static files */
			"/webjars/**",
			"/assets/**",
			"/static/",
			"/",
			"/logout",
			"/index.html",
			"/vendor/graphiql/**.js",

			/* Documentation */
			"/v3/api-docs/**",
			"/api-docs/**",
			"/swagger-ui/**",

			"/{x:(?!api|graphql|voyager|altair|graphiql|swagger-ui.html).*}",
			// % protected region % [Add any custom HTTP GET whitelist routes here] off begin
			// % protected region % [Add any custom HTTP GET whitelist routes here] end
	};

	/**
	 * Allow anonymous access for requests using the HTTP POST verb for these endpoints.
	 */
	public static final String[] ANONYMOUS_POST_AUTH_WHITELIST = {
			"/api/authorization/request-reset-password",
			"/graphql",
			// % protected region % [Add any custom HTTP POST whitelist routes here] off begin
			// % protected region % [Add any custom HTTP POST whitelist routes here] end
	};

	/**
	 * Allow anonymous access for requests using the HTTP PUT verb for these endpoints.
	 */
	public static final String[] ANONYMOUS_PUT_AUTH_WHITELIST = {
			// % protected region % [Add any custom HTTP PUT whitelist routes here] off begin
			// % protected region % [Add any custom HTTP PUT whitelist routes here] end
	};

	/**
	 * Key used for anonymous user if not authenticated. Note that this key is not configurable.
	 */
	public static final String ANONYMOUS_KEY = "anonymous";

	/**
	 * Username used for anonymous user if not authenticated. Note that this username is not configurable.
	 */
	public static final UserDetails ANONYMOUS_USERNAME = new User("anonymous@example.com", "anonymous", List.of());

	/**
	 * List of all roles that are applied to anonymous user. By default anonymous user is no different than normal a
	 * unauthenticated user.
	 */
	public static final List<GrantedAuthority> ANONYMOUS_ROLES = ImmutableList.of(
		// % protected region % [Add any additional public roles here] off begin
		// % protected region % [Add any additional public roles here] end
		// Stub authority to satisfy Spring security. This authority does not do anything and is not meant to be used
		// in any meaningful way.
		new CustomGrantedAuthority("ROLE_ANONYMOUS", "", false, false, false, false)
	);

	/**
	 * The anonymous user which will be set by default if there is no logged in user.
	 */
	public static final Authentication ANONYMOUS_USER = new AnonymousAuthenticationToken(
		ANONYMOUS_KEY,
		ANONYMOUS_USERNAME,
		ANONYMOUS_ROLES
	);

	/*
	 * Environment info.
	 */
	private boolean isDevEnvironment;
	private boolean isTestEnvironment;

	/*
	 * Authentication and security info.
	 */
	private final AuthenticationService authService;
	private final RestAuthenticationEntryPoint restAuthenticationEntryPoint;

	/*
	 * User management services.
	 */
	private final UserService userService;

	/*
	 * Base user repository
	 */
	private final UserRepository userRepository;
	private final RoleRepository roleRepository;

	/*
	 * Login and logout handlers.
	 */
	private final AuthenticationSuccessHandler authSuccessHandler;
	private final AuthenticationFailureHandler authFailureHandler;
	private final LogoutSuccessHandler logoutHandler;

	private final XsrfTokenRepository xsrfTokenRepository;

	// % protected region % [Add any additional class fields here] off begin
	// % protected region % [Add any additional class fields here] end

	@Autowired
	public SecurityConfig(
			// % protected region % [Add any additional constructor parameters here] off begin
			// % protected region % [Add any additional constructor parameters here] end
			Environment env,
			AuthenticationService authService,
			RestAuthenticationEntryPoint restAuthenticationEntryPoint,
			UserService userService,
			UserRepository userRepository,
			RoleRepository roleRepository,
			AuthenticationSuccessHandler authSuccessHandler,
			AuthenticationFailureHandler authFailureHandler,
			LogoutSuccessHandler logoutHandler,
			XsrfTokenRepository xsrfTokenRepository
	) {
		// % protected region % [Add any additional constructor logic before the main body here] off begin
		// % protected region % [Add any additional constructor logic before the main body here] end

		for (int i = 0; i < env.getActiveProfiles().length; ++i) {
			if (env.getActiveProfiles()[i].equals("dev")) {
				isDevEnvironment = true;
			} else if (env.getActiveProfiles()[i].equals("test")) {
				isTestEnvironment = true;
			}
		}

		// % protected region % [Add any additional constructor logic here] off begin
		// % protected region % [Add any additional constructor logic here] end

		this.authService = authService;
		this.restAuthenticationEntryPoint = restAuthenticationEntryPoint;
		this.userService = userService;
		this.userRepository = userRepository;
		this.roleRepository = roleRepository;
		this.authSuccessHandler = authSuccessHandler;
		this.authFailureHandler = authFailureHandler;
		this.logoutHandler = logoutHandler;
		this.xsrfTokenRepository = xsrfTokenRepository;

		// % protected region % [Add any additional constructor logic after the main body here] off begin
		// % protected region % [Add any additional constructor logic after the main body here] end
	}

	/**
	 * @inheritDoc
	 */
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		// % protected region % [Add any additional security configuration before the main body here] off begin
		// % protected region % [Add any additional security configuration before the main body here] end

		if (isDevEnvironment || isTestEnvironment) {
			// % protected region % [Add any additional security configuration for dev and test environments before the main body here] off begin
			// % protected region % [Add any additional security configuration for dev and test environments before the main body here] end

			// % protected region % [Override the CORS configuration here for test and development] off begin
			var corsConfiguration = new CorsConfiguration().applyPermitDefaultValues();
			corsConfiguration.setAllowedOrigins(Arrays.asList("http://localhost:4200", "http://localhost:8000"));
			corsConfiguration.addAllowedMethod(HttpMethod.OPTIONS);
			corsConfiguration.setAllowCredentials(true);
			// % protected region % [Override the CORS configuration here for test and development] end

			http
				.csrf()
				.ignoringAntMatchers(new String[] {
					"/graphql",
					"/api/**"
				})
				.and()
				.cors().configurationSource(request -> corsConfiguration)
				.and()
				.authorizeRequests()
				.antMatchers(DEV_AUTH_WHITELIST).permitAll()
				.and()
				// Allow browsers to load the h2 console
				.headers().frameOptions().sameOrigin();

			// % protected region % [Add any additional security configuration for dev and test environments after the main body here] off begin
			// % protected region % [Add any additional security configuration for dev and test environments after the main body here] end
	} else {
			// % protected region % [Add any additional security configuration for other environments before the main body here] off begin
			// % protected region % [Add any additional security configuration for other environments before the main body here] end

			// % protected region % [Override strict HTTPS mode here] off begin
			http
					.requiresChannel()
					.antMatchers("/**").requiresSecure();
			// % protected region % [Override strict HTTPS mode here] end

			// % protected region % [Add any additional security configuration for other environments after the main body here] off begin
			// % protected region % [Add any additional security configuration for other environments after the main body here] end
		}

		// % protected region % [Add any additional security configuration here] off begin
		// % protected region % [Add any additional security configuration here] end

		http
				.csrf().csrfTokenRepository(xsrfTokenRepository)
				.ignoringAntMatchers(new String[] {
					"/auth/login",
					"/api/authorization/reset-password",
					"/api/authorization/request-reset-password",
				})
				.and()
				.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
				.and()
				.authorizeRequests()
				.antMatchers(AUTH_WHITELIST).permitAll()
				.antMatchers(HttpMethod.GET, ANONYMOUS_GET_AUTH_WHITELIST).permitAll()
				.antMatchers(HttpMethod.POST, ANONYMOUS_POST_AUTH_WHITELIST).permitAll()
				.antMatchers(HttpMethod.PUT, ANONYMOUS_PUT_AUTH_WHITELIST).permitAll()
				.anyRequest().authenticated()
				.and()
				.formLogin()
					.loginPage("/login")
					.loginProcessingUrl("/auth/login")
					.successHandler(authSuccessHandler)
					.failureHandler(authFailureHandler)
				.and()
				.logout()
					.logoutUrl("/auth/logout")
					.logoutSuccessHandler(logoutHandler)
					.deleteCookies(AuthenticationService.AUTH_TOKEN_COOKIE_NAME)
					.deleteCookies(XsrfTokenRepository.XSRF_TOKEN_COOKIE_NAME)
				.and()
				// % protected region % [Add any additional filters before the main ones here] off begin
				// % protected region % [Add any additional filters before the main ones here] end
				.addFilterBefore(
						new ResetPasswordFilter(
								"/api/authorization/reset-password",
								userService,
								authSuccessHandler,
								passwordEncoder(),
								authenticationManager()
						),
						UsernamePasswordAuthenticationFilter.class
				)
				.addFilterBefore(
						new JwtAuthenticationFilter("/graphql", authService),
						UsernamePasswordAuthenticationFilter.class)
				.addFilterBefore(
						new JwtAuthenticationFilter("/api/(?!api-docs).*", authService),
						UsernamePasswordAuthenticationFilter.class)
				.addFilterBefore(
						new JwtAuthenticationFilter("/actuator/info", authService),
						UsernamePasswordAuthenticationFilter.class)
				// % protected region % [Add any additional filters after the main ones here] off begin
				// % protected region % [Add any additional filters after the main ones here] end
				.exceptionHandling()
				.authenticationEntryPoint(restAuthenticationEntryPoint);

		// % protected region % [Add any additional security configuration after the main body here] off begin
		// % protected region % [Add any additional security configuration after the main body here] end
	}

	/**
	 * @inheritDoc
	 */
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		// % protected region % [Add any additional logic for configure before the main body here] off begin
		// % protected region % [Add any additional logic for configure before the main body here] end

		auth
				// % protected region % [Add any additional configurations for the authentication manager here] off begin
				// % protected region % [Add any additional configurations for the authentication manager here] end
				.authenticationProvider(authenticationProvider())
				.jdbcAuthentication();

		// % protected region % [Add any additional logic for configure after the main body here] off begin
		// % protected region % [Add any additional logic for configure after the main body here] end
	}

	/**
	 * Authentication provider to be used in the authentication process. This differs from the default provider due to
	 * the custom {@link UserService} that handles authenticating users.
	 */
	@Bean
	public DaoAuthenticationProvider authenticationProvider() {
		// % protected region % [Add any additional logic for authenticationProvider before the main body here] off begin
		// % protected region % [Add any additional logic for authenticationProvider before the main body here] end

		DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
		authProvider.setUserDetailsService(userService);
		authProvider.setPasswordEncoder(passwordEncoder());

		// % protected region % [Add any additional logic for authenticationProvider after the main body here] off begin
		// % protected region % [Add any additional logic for authenticationProvider after the main body here] end

		return authProvider;
	}

	/**
	 * Password encoder used to encode user password during registration process.
	 */
	@Bean
	public PasswordEncoder passwordEncoder() {
		// % protected region % [Update password encoder configuration here] off begin
		return new BCryptPasswordEncoder(11);
		// % protected region % [Update password encoder configuration here] end
	}

	// % protected region % [Update request logging configuration here] off begin
	/**
	 * Filter used to log request info for every request that reaches the server side.
	 */
	@Bean
	public RequestLoggingFilter logFilter() {
		return new RequestLoggingFilter();
	}
	// % protected region % [Update request logging configuration here] end

	/**
	 * For all cookies contained with the response, set the sameSite=Strict attribute
	 *
	 * @see <a href="https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Set-Cookie/SameSite">SameSite</a>
	 * @see <a href="https://github.com/spring-projects/spring-security/issues/7537">CSRF Support for SameSite #7537</a>
	 *
	 * @param response The response that contains the cookies
	 */
	public static void setSameSiteAttributeForAllCookies(HttpServletResponse response) {
		// % protected region % [Override the sameSite attribute for all cookies here] off begin

		// Please note that this will set duplicate "SET-COOKIE" headers with the
		// second one having the SameSite attribute set correctly.
		// This will result in the correct attributes being stored.
		response.getHeaders(HttpHeaders.SET_COOKIE).stream().forEach((header) -> {
			if (!header.contains("SameSite=Strict")) {
				response.addHeader(HttpHeaders.SET_COOKIE, String.format("%s; %s", header, "SameSite=Strict"));
			}
		});
		// % protected region % [Override the sameSite attribute for all cookies here] end
	}


	// % protected region % [Add any additional class methods here] off begin
	// % protected region % [Add any additional class methods here] end
}
